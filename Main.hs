{-# LANGUAGE OverloadedStrings #-}

module Main where

import Build
import Build.Store
import Build.System
import Build.Task
import Build.Trace
import Control.Monad.State
import qualified Data.Map as Map
import Data.Monoid (Monoid, mempty)

sprsh2 :: Tasks Applicative String Integer
sprsh2 "b1" = Just $ Task $ \fetch -> fetch "b2"
sprsh2 "b2" = Just $ Task $ \fetch -> ((+) <$> fetch "a1" <*> fetch "a2")
sprsh2 "b3" = Just $ Task $ \fetch -> fetch "a1"
sprsh2 _ = Nothing

trace :: VT String Integer
trace = mempty

m :: Map.Map String Integer
m = Map.fromList [("a1", 10)]

sp2store =
  initialise
    trace
    ( \key ->
        case Map.lookup key m of
          Just value -> value
          Nothing -> 0
    )

result2 = ninja sprsh2 "b2" sp2store

main :: IO ()
main = do
  putStrLn $ "A1: " ++ show (getValue "a1" result2) ++ " expected: 10"
  putStrLn $ "B1: " ++ show (getValue "b1" result2) ++ " expected: 10"
  putStrLn $ "B2: " ++ show (getValue "b2" result2) ++ " expected: 10"
  putStrLn $ "B3: " ++ show (getValue "b3" result2) ++ " expected: 10"
  putStrLn $ "C1: " ++ show (getValue "c1" result2) ++ " expected: 0"
